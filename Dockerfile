FROM blalabs/debian:jessie

MAINTAINER cas@bla.pm
EXPOSE 80

ENV DEBIAN_FRONTEND noninteractive

ADD root /

RUN echo "** ensure apt-get cache is updated **" && \
		apt-get update
RUN echo "** ensure nagios packages are installed **" && \
		apt-get install -y nagios3 nagios-plugins nagios-nrpe-plugin apache2 runit apt-transport-https wget openssh-client python3 python3-pip \
		libnagios-plugin-perl nagios-nrpe-server nagios-plugins-standard php5-curl

RUN echo "** ensure pagerduty agent is installed **" && \
		wget -O - https://packages.pagerduty.com/GPG-KEY-pagerduty | apt-key add - && \
		echo "deb https://packages.pagerduty.com/pdagent deb/" >/etc/apt/sources.list.d/pdagent.list && \
		apt-get update && mv /bin/systemctl /bin/systemctl.old && apt-get install pdagent pdagent-integrations -y && mv /bin/systemctl.old /bin/systemctl

RUN echo "** copy original nagios3 files **" && \
		mkdir -p /orig/etc/nagios3 /orig/var/lib/nagios3 && \
		cp -Rp /etc/nagios3/* /orig/etc/nagios3 && \
		cp -Rp /var/lib/nagios3/* /orig/var/lib/nagios3

RUN echo "** www-data user is in nagios group **" && \
		usermod -aG nagios www-data
RUN echo "** ensure apache2 rewrite module is enabled **" && \
		a2enmod rewrite

VOLUME /etc/nagios3 /var/lib/nagios3
